<?php

use yii\db\Migration;

/**
 * Class m180207_044817_file
 */
class m180207_044817_file extends Migration
{
    public function up()
    {
     $this->createTable('file', [
                'id' => $this->primaryKey(),
                'id_news'=>$this->integer(),
                'name_file' => $this->string(),
            ]);
        $this->createIndex(
            'index_news_id',
            'file',
            'id_news'
        );
        $this->addForeignKey(
            'team_to_file',
            'file',
            'id_news',
            'news',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%file}}');
    }
}
