<?php

use yii\db\Migration;

/**
 * Class m180207_025918_news
 */
class m180207_025918_news extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'title'=>$this->string(),
            'body' => $this->string(),
            'created_at' =>$this->dateTime(),
            'updated_at' =>$this->dateTime(),
            'status' =>$this->integer(),
            'number_files' => $this->integer(),
            'image' => $this->string()
        ]);
        $this->createIndex(
            'index_user_id',
            'news',
            'id_user'
        );
        $this->addForeignKey(
            'team_to_news',
            'news',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );
    }

        public function down()
        {
            $this->dropTable('{{%news}}');
        }

}
