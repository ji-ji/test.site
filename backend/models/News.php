<?php


namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\User;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $id_user
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 * @property int $number_files
 * @property string $image
 *
 * @property File[] $files
 * @property User $user
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $downFiles;
    public $imageFile;
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }


    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'status', 'number_files'], 'integer'],
            [['title', 'body', 'image'], 'string','max' => 255],
            [['body'], 'string','max' => 2555],
            [['title','body'], 'required'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['downFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, zip, doc, docx', 'maxFiles' => 0,'maxSize'=>1024 * 1024 * 4],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок статьи',
            'body' => 'Текст',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата последнего обновления',
            'status' => 'Статус ',
            'number_files' => 'Кол-во прикрепленных файлов',
            'image' => 'Изображения ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['id_news' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function upload()
    {
        $dir = Yii::getAlias('@frontend').'/web/uploads/';

        if ($this->validate()) {

            foreach ($this->downFiles as $file) {
                $file->saveAs($dir.$file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }

    public function upload_img()
    {
        $dir = Yii::getAlias('@frontend').'/web/uploads/';
        if( $this->imageFile){
            $this->imageFile->saveAs($dir. $this->imageFile->baseName . '.' . $this->imageFile->extension);
        }
    }

    public function getImagesLinksData(){
        return ArrayHelper::toArray($this->files,[
                File::className()=>[
                'caption' => 'name_file',
                'key' => 'id'
            ]]
        );
    }
}
