<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;

for($i=0;$i<count($model_file);$i++){
    $html_img[$i] = "<img src='/uploads/".$model_file[$i]['name_file']."' alt title='2342341' style='width: 210px'>";
    $html_title[$i] =  ['caption' => $model_file[$i]['name_file'],'key'=>$model_file[$i]['id']];
}

?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-9">
            <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' =>['enctype'=>'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'body')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>

            <?= $form->field($model, 'status')->checkbox([
                'label'=>'Снять с публикации/опубликовать',
                'data-toggle'=>'toggle'
            ]) ?>

            <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
                'name' => 'input-ru[]',
                'language' => 'ru',
                'options' => ['accept' => 'png, jpg , gif','multiple' => true ],
                'pluginOptions' => [
                    'initialPreview'=>[
                        Html::img("/uploads/" . $model->image,['class' => 'imagg'])
                    ],

                    'overwriteInitial'=>true,
                    'previewFileType' => 'any',
                    'uploadUrl' => Url::to(['/site/file-upload']),


                ],
            ]);
            ?>

            <?= $form->field($model, 'downFiles[]')->widget(FileInput::classname(), [
                'name' => 'input-ru[]',
                'options' => ['accept' => 'pdf, zip, doc, docx','multiple' => true],
                'pluginOptions' => [
                    'deleteUrl' => Url::toRoute(['/news/delete-ajax']),
                    'initialPreview'=>$html_img,
                    'initialPreviewAsData'=>true,
                    'overwriteInitial'=>false,
                    'initialPreviewConfig'=> $model->getImagesLinksData(),
                    'uploadUrl' => Url::to(['/news/file-ajax']),
                    'previewFileType' => 'any'
                ],
            ]);
            ?>




            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>

