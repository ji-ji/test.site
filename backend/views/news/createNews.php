<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
    </p>

    <div class="row">
        <div class="col-lg-9">
            <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' =>['enctype'=>'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'body')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>

            <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
                'name' => 'input-ru[]',
                'language' => 'ru',
                'options' => ['accept' => 'pdf, zip, doc, docx','multiple' => true],
                'pluginOptions' => ['previewFileType' => 'any']
            ]);
            ?>

            <?= $form->field($model, 'downFiles[]')->widget(FileInput::classname(), [
                'name' => 'input-ru[]',
                'language' => 'ru',
                'options' => ['accept' => 'pdf, zip, doc, docx','multiple' => true],
                'pluginOptions' => ['previewFileType' => 'any']
            ]);
            ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>

