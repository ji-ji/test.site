<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
/*echo "<pre>";
print_r($model);
echo "</pre>";*/
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create-news'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'created_at',
            'updated_at',
            [   'attribute' => 'status',
                'format' => 'raw',
                'filter' => [
                    1 => 'Активен',
                    0 => 'Завершен',
                ],
                'value' => function ($data) {
                    $status = $data->status;
                    return $status ? '<span class =\'label label-success\'>Опубликован</span>':'<span class =\'label label-danger\'>Не опубликован</span>';
                },
            ],
            //'status',
            //'number_files',
            [   'attribute' => 'number_files',
                'format' => 'raw',
                'filter' => [
                    2 => '2',
                    3 => '3',
                    4 => '4',
                    5 => '5',
                    6 => '6',
                    9 => '9',
                    15 => '15',
                    20 => '20',
                ],
            ],
            //'image',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {update-news} {all}',
                'buttons' => [
                    'update-news' => function ($url, $model, $key) {
                        return Html::a('<span class="	glyphicon glyphicon-pencil">',
                            ['update-news', 'id'=>$model->id]);
                    },
                    'all' => function ($url, $model, $key) {
                        return ButtonDropdown::widget([
                            'encodeLabel' => false,
                            'label' => 'Статус',
                            'dropdown' => [
                                'encodeLabels' => false,
                                'items' => [
                                    [
                                        'label' => \Yii::t('yii', 'Опубликовать'),
                                        'url' => ['publish', 'id' => $key],
                                    ],
                                    [
                                        'label' => \Yii::t('yii', 'С публикации'),
                                        'url' => ['from-publication', 'id' => $key],
                                        'visible' => true,
                                    ],
                                ],
                                'options' => [
                                    'class' => 'dropdown-menu-right',
                                ],
                            ],
                            'options' => [
                                'class' => 'btn-default',
                            ],
                            'split' => false,
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
