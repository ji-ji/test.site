<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\News */
$dir = Yii::getAlias('@frontend').'/uploads/';
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update-news', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            'created_at',
            'updated_at',
            [   'attribute' => 'status',
                'format' => 'raw',
                'filter' => [
                    1 => 'Опубликован',
                    0 => 'Не опубликован',
                ],
                'value' => function ($data) {
                    $status = $data->status;
                    return $status ? '<span class =\'label label-success\'>Опубликован</span>':'<span class =\'label label-danger\'>Не опубликован</span>';
                },
            ],
            [
                'attribute'=>'image',
                'value'=> '/uploads/'.$model->image,
                'format' => ['image',['width'=>'30%']],
            ],
        ],
    ]) ?>

</div>
<div>
    <div class="container">
        <ul class="list-group">
            <li class="list-group-item active"> Кол-во прикрепленных файлов</li>
            <?php foreach($model_file as $item):?>
                <li class="list-group-item"><?=$item->name_file?> </li>
            <?php endforeach;?>
            </ul>
    </div>
</div>