<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\UploadedFile;
use app\models\News;
use yii\web\NotFoundHttpException;
use app\models\NewsSearch;
use app\models\File;

class NewsController extends Controller
{
    public $file_download;


    public function actionFileAjax()
    {
        if(Yii::$app->request->isAjax){

            return 11111111;
        }

        return 22222222;
    }

    public function actionPublish($id){
        $model = $this->findModel($id);
        $model->status = '1';
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionFromPublication($id){
        $model = $this->findModel($id);
        $model->status = '0';
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionDeleteAjax()
    {
        $dir = Yii::getAlias(   '@frontend').'/web/uploads/';

        if(Yii::$app->request->isAjax){
            $model= File::findOne((Yii::$app->request->post('key')));
            $model_news = News::findOne($model->id_news);
            $model_news->number_files = --$model_news->number_files;
            $model_news->save();
            return $model->delete();
        }

        return false;
    }



    public function actionView($id)
    {     $model_file = File::find()
        ->where(['id_news' => $id])
        ->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'model_file' => $model_file,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new News();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
          ]);
    }

    public function actionUpdateNews($id)
    {
        $model = News::findOne($id);
        $model_file = File::find()
            ->where(['id_news' => $id])
            ->all();

        if ($model->load(Yii::$app->request->post())) {
            $model->downFiles = UploadedFile::getInstances($model, 'downFiles');
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if($model->imageFile){
                $model->image = $model->imageFile->name;
            }
            $model->number_files += count($model->downFiles);
            $model->save();
            $model->upload();
            $model->upload_img();
            $this->saveGalaryItems($model->downFiles,$model->id);

            Yii::$app->session->setFlash('success','Your article update '.$model->title.' !!!!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('updateNews', [
            'model' => $model,
            'model_file'=>$model_file
        ]);
    }


    public function actionCreateNews()
    {
        $model = new News();
        if($model->load(Yii::$app->request->post())){

            $model->downFiles = UploadedFile::getInstances($model, 'downFiles');
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
           // print_r( $model->imageFile->name);return;
            $model->status = '0';
            $model->id_user = $_SESSION['__id'];
            $model->number_files = count($model->downFiles);
            $model->image = $model->imageFile->name;

            $model->save();
            $model->upload();
            $model->upload_img();
            $this->saveGalaryItems($model->downFiles,$model->id);

            Yii::$app->session->setFlash('success','Your article save '.$model->title.' !!!!');

            return $this->redirect(['/news']);
        }else {
            return $this->render('createNews', [
                'model' => $model,
            ]);
        }

    }


    protected function saveGalaryItems($items,$news_Id)
    {
        foreach ($items as $tems_galary)
        {
            $gallery_items = new File();
            $gallery_items->id_news = $news_Id;
            $gallery_items->name_file = $tems_galary->name;
            $gallery_items->save();
        }
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}